# -*- coding: utf-8 -*-

import math
import random
import pickle
from pylab import ylim, show, plot, ion

MIN = -1.0
MAX = 1.0


class PerceptronMulticapa:
    def __init__(self, cantidad_entradas, cantidad_salidas, neuronas_capas, funciones_activacion_capas, funcion_activacion_salida):
        self.m = cantidad_entradas
        self.n = cantidad_salidas
        self.neuronas_capas = neuronas_capas # [100, 80, ...]
        self.funciones_activacion_capas = funciones_activacion_capas # ['logsig', 'logsig', ...]
        self.cantidad_capas = len(neuronas_capas)
        self.funciones_activacion_salida = funcion_activacion_salida # 'purelin'
        self.cantidad_patrones = None # Cantidad de patrones
        self.errores = []
        self.inicializar_pesos()
        self.inicializar_umbrales()
        self.inicializar_parametros_de_entrenamiento()

    def inicializar_pesos(self):
        self.wc = []
        for c in range(self.cantidad_capas + 1):
            if c == 0:
                self.wc.insert(len(self.wc), [[random.uniform(MIN, MAX) for i in range(self.neuronas_capas[c])] for j in range(self.m)])
            elif c == self.cantidad_capas:
                self.wc.append([[random.uniform(MIN, MAX) for i in range(self.n)] for j in range(self.neuronas_capas[c - 1])])
            else:
                self.wc.append([[random.uniform(MIN, MAX) for i in range(self.neuronas_capas[c])] for j in range(self.neuronas_capas[c-1])])
        self.imprimir_pesos()

    def imprimir_pesos(self):
        for c in range(len(self.wc)):
            print("%s MATRIZ DE PESOS %s %s" % ('*'*30, (c + 1), '*'*30))
            pesos = self.wc[c]
            for i in range(len(pesos[0])):
                row = []
                for j in range(len(pesos)):
                    row.append(pesos[j][i])
                print(row)

    def inicializar_umbrales(self):
        self.uc = []
        for capa in range(self.cantidad_capas):
            self.uc.insert(len(self.uc), [random.uniform(-1.0, 1.0) for i in range(self.neuronas_capas[capa])])
        self.uc.insert(len(self.uc), [random.uniform(-1.0, 1.0) for i in range(self.n)])
        self.imprimir_umbrales()

    def imprimir_umbrales(self):
        for c in range(len(self.uc)):
            print("%s VECTOR DE UMBRALES %s %s" % ('*'*30, (c + 1), '*'*30))
            print(self.uc[c])

    def definir_parametos_auxiliares(self):
        self.h = [[0 for j in range(self.neuronas_capas[i])] for i in range(self.cantidad_capas)]
        self.y = [[0.0 for p in range(self.cantidad_patrones)] for i in range(self.n)] # y[[salida 1], [salida 2], ...]
        self.el = [[0.0 for i in range(self.n)] for p in range(self.cantidad_patrones)]
        self.ep = [0.0 for p in range(self.cantidad_patrones)] # ep[errores...]
        self.rms = 0.0

    def inicializar_parametros_de_entrenamiento(self):
        self.trainParam = {}
        self.trainParam['epochs'] = 1000
        self.trainParam['goal'] = 0.00
        self.trainParam['lr'] = 0.01

    def definir_cantidad_patrones(self, cantidad_patrones = None):
        self.cantidad_patrones = len(self.x) if cantidad_patrones == None else cantidad_patrones

    def calcular_salida_de_la_red(self, patron):
        for c in range(self.cantidad_capas):
            entradas = self.m if c == 0 else self.neuronas_capas[c - 1]
            for i in range(self.neuronas_capas[c]):
                output = 0.0
                for j in range(entradas):
                    output += self.x[patron][j] * self.wc[c][j][i]
                output -= self.uc[c][i]
                self.h[c][i] = self.aplicar_funcion_de_activacion(self.funciones_activacion_capas[c], output)
        for i in range(self.n):
            output = 0.0
            for j in range(self.neuronas_capas[self.cantidad_capas - 1]):
                output += self.x[patron][j] * self.wc[len(self.wc) - 1][j][i]
            output -= self.x[patron][j] * self.uc[len(self.uc) - 1][i]
            self.y[i][patron] = self.aplicar_funcion_de_activacion(self.funciones_activacion_salida, output)
        self.mostrar_salidas_calculadas_en_la_red(patron)

    def mostrar_salidas_calculadas_en_la_red(self, patron):
        for c in range(self.cantidad_capas):
            print('%s SALIDAS DE LA CAPA (H) %s DEL PATRON %s %s' % ('='*30, c + 1, patron + 1, '='*30))
            row = []
            for i in range(len(self.h[c])):
                row.append(self.h[c][i])
        for i in range(len(self.y)):
            row.append(self.y[i][patron])
        print(row)

    def aplicar_funcion_de_activacion(self, capa, valor):
        if capa == 'tanh':
            return math.tanh(valor)
        elif capa == 'sigmoid':
            return 1 / (1 + math.exp(-valor))


    def calcular_error_lineal(self, patron):
        for i in range(self.n):
            self.el[patron][i] = self.yd[i][patron] - self.y[i][patron]
        self.mostrar_errores_lineales_del_patron(patron)

    def mostrar_errores_lineales_del_patron(self, patron):
        print('%s ERRORES LINEALES DEL PATRON %s %s' % ('%'*30, patron + 1, '%'*30))
        print(self.el[patron])

    def calcular_error_del_patron(self, patron):
        self.ep[patron] = sum([abs(e) for e in self.el[patron]]) / self.n           
        self.mostrar_errores_por_patron(patron)

    def mostrar_errores_por_patron(self, patron):
        print('%s ERRORES PRODUCIDOS EN EL PATRON %s %s' % ('*'*10, patron + 1, '*'*10))
        print(self.ep[patron])

    def actualizar_pesos_y_umbrales(self, patron):
        for c in range(len(self.wc)):
            print('%s ACTUALIZANDO PESOS DE LA MATRIZ %s %s' % ('-'*15, c + 1, '-'*15))
            if c == 0:
                for i in range(self.neuronas_capas[c]):
                    for j in range(self.m):
                        self.wc[c][j][i] += self.trainParam['lr'] * self.ep[patron] * self.x[patron][j]
                    self.uc[c][i] += self.trainParam['lr'] * self.ep[patron] * 1
            elif c == len(self.wc) - 1:
                for i in range(self.n):
                    for j in range(self.neuronas_capas[len(self.neuronas_capas) - 1]):
                        self.wc[c][j][i] += self.trainParam['lr'] * self.el[patron][i] * self.y[i][patron]
                    self.uc[c][i] += self.trainParam['lr'] * self.el[patron][i] * 1
            else:
                for i in range(self.neuronas_capas[c]):
                    for j in range(self.neuronas_capas[c - 1]):
                        self.wc[c][j][i] += self.wc[c][j][i] * self.ep[patron] * self.h[c - 1][j]
                    self.uc[c][i] += self.trainParam['lr'] * self.ep[patron] * 1
        self.mostrar_pesos_y_umbrales_actualizados()

    def mostrar_pesos_y_umbrales_actualizados(self):
        pass

    def calcular_error_rms(self):
        self.rms = sum(self.ep) / self.cantidad_patrones
        self.errores.append(self.rms)
        print('RMS: %s' % self.rms)

    def graficar_errores(self):
        ylim(-1,1)
        plot(self.errores)
        show()

    def guardar_pesos_y_umbrales(self):
        pickle.dump(self.wc, open('pesos.db', 'wb'))
        pickle.dump(self.uc, open('umbrales.db', 'wb'))

    def entrenar(self, patrones, salida_deseada):
        self.x = patrones # x[[patron 1], [patron 2], ...]
        self.yd = salida_deseada # y[[salida 1], [salida 2], ...]
        self.definir_cantidad_patrones()
        self.definir_parametos_auxiliares()
        for iteracion in range(self.trainParam['epochs']):
            for patron in range(self.cantidad_patrones):
                self.calcular_salida_de_la_red(patron)
                self.calcular_error_lineal(patron)
                self.calcular_error_del_patron(patron)
                self.actualizar_pesos_y_umbrales(patron)
            self.calcular_error_rms()
            if self.rms <= self.trainParam['goal']:
                self.graficar_errores()
                self.guardar_pesos_y_umbrales()
                break
            elif iteracion >= 999:
                self.graficar_errores()

    def simular(self, entradas):
        pass

entradas = [
    [2, 2, 2, 2, 10, 10, 2, 2, 2, 2],
    [10,6, 6, 6,  6,  6, 6, 6, 6, 6],
    [10,4, 4, 4,  4,  4, 4, 4, 4, 4],
    [2, 2, 2, 5, 10, 10, 7, 8, 2, 2],
    [10, 10, 6, 4, 4,6,  6, 6, 6, 6]
]
salidas = [
    [1, 0, 0, 0, 0]
]

red = PerceptronMulticapa(len(entradas[0]), len(salidas), [3, 2], ['tanh', 'tanh'], 'tanh')
red.trainParam['epochs'] = 1000
red.entrenar(entradas, salidas)
# salida_red = red.simular(entradas)
